#include <cstdio>
#include <vector>
#include <algorithm>
#include "lib/Prime.h"
using namespace std;

struct PhiSome {
    vector<int> p;
    int n;

    PhiSome(int n_): n(n_) {
        p = Prime::factorize(n);
        p.resize(unique(p.begin(), p.end()) - p.begin());
    }

    int f(int k, int m) {
        if (k >= (int)p.size()) return m;
        return f(k + 1, m) - f(k + 1, m / p[k]);
    }

    int thirdToHalf() {
        return f(0, (n - 1) / 2) - f(0, n / 3);
    }
};

int main() {
    int sum = 0;
    for (int i = 1; i <= 12000; ++i) sum += PhiSome(i).thirdToHalf();
    printf("%d\n", sum);
    return 0;
}
