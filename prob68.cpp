#include <cstdio>
#include <initializer_list>
using namespace std;

struct MagicRing {
    int a[10], s;

    bool search(int k) {
        if (k >= 10) return true;
        int uB = 9, lB = 1;
        switch (k) {
        case 0:
            uB = 6; break;
        case 3: case 5: case 7: case 9:
            lB = a[0];
        }
        for (int ak = uB; ak >= lB; --ak) {
            a[k] = ak;
            switch (k) {
            case 3: case 5: case 7: case 9:
                if (a[k] == lB) a[k] = 10;
            }
            bool valid = true;
            for (int i = 0; i < k; ++i)
                if (a[i] == a[k]) { valid = false; break; }
            if (!valid) continue;
            switch (k) {
                case 2:
                    s = a[0] + a[1] + a[2]; break;
                case 4: case 6: case 8:
                    if (s != a[k] + a[k - 1] + a[k - 2]) valid = false;
                    break;
                case 9:
                    if (s != a[1] + a[8] + a[9]) valid = false;
            }
            if (!valid) continue;
            if (search(k + 1)) return true;
        }
        return false;
    }
};

int main() {
    MagicRing mr;
    mr.search(0);
    for (int i: {0, 1, 2, 3, 2, 4, 5, 4, 6, 7, 6, 8, 9, 8, 1})
        printf("%d", mr.a[i]);
    printf("\n");
    return 0;
}
