#include "lib/Fibonacci.h"
#include "lib/Pandigital.h"
#include <cstdio>
using namespace std;

int main() {
    int count = 1;
    Fibonacci<double> aD;
    Fibonacci<int> aI;
    for (;; ++count) {
        char aS[10];
        sprintf(aS, "%.0f", *aD);
        int c[10]{};
        for (int i = 0; aS[i]; ++i) ++c[aS[i] - '0'];
        if (Pandigital::is(c)) {
            sprintf(aS, "%.0d", *aI);
            for (int i = 0; i < 10; ++i) c[i] = 0;
            for (int i = 0; aS[i]; ++i) ++c[aS[i] - '0'];
            if (Pandigital::is(c)) break;
        }
        if (*++aD >= 1000000000.0) {
            aD.a1 /= 10.0;
            aD.a2 /= 10.0;
        }
        (++aI).a2 %= 1000000000;
    }
    printf("%d\n", count);
    return 0;
}
