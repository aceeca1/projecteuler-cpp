#include <cstdio>
#include <gmpxx.h>
#include "lib/TortoiseHare.h"
using namespace std;

struct Base10Mod {
    int m;
    void operator()(int& n) const { n = n * 10 % m; }
};

int main() {
    int max = 0, maxI = -1;
    for (int i = 1; i < 1000; ++i) {
        int n = TortoiseHare::len<int>(1, Base10Mod{i});
        if (n > max) { max = n; maxI = i; }
    }
    printf("%d\n", maxI);
    return 0;
}
