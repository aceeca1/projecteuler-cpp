#include <cstdio>
#include <vector>
#include "lib/Factorial.h"
using namespace std;

int main() {
    auto fac = Factorial::get();
    vector<int> a(fac[9] * 7 + 1);
    int count = 0;
    for (int i = 1; i < 1000000; ++i) {
        if (a[i]) continue;
        vector<int> b;
        int j = i, no = -1;
        for (; !a[j]; --no) {
            a[j] = no;
            b.emplace_back(j);
            char s[10];
            sprintf(s, "%d", j);
            j = 0;
            for (int k = 0; s[k]; ++k) j += fac[s[k] - '0'];
        }
        int no1, cyc;
        if (a[j] > 0) { no1 = b.size(); cyc = a[j]; }
        else { no1 = ~a[j]; cyc = ~no - no1; }
        for (int k = b.size() - 1; k >= no1; --k) a[b[k]] = cyc;
        for (int k = no1 - 1; k >= 0; --k) a[b[k]] = ++cyc;
    }
    for (int i = 1; i < 1000000; ++i) {
        if (a[i] != 60) continue;
        ++count;
    }
    printf("%d\n", count);
    return 0;
}
