#include <cstdio>
#include <cmath>
#include <gmpxx.h>
using namespace std;

struct PellEqn {
    int n;
    mpz_class p, q;

    bool isSolution(mpz_class& pi, mpz_class& qi) {
        return pi * pi - qi * qi * n == 1;
    }

    PellEqn(int n_): n(n_) {
        int nS = sqrt(n);
        if (nS * nS == n) return;
        mpz_class p0 = nS, q0 = 1;
        if (isSolution(p0, q0)) { p = p0; q = q0; return; }
        int u1 = nS, v1 = n - nS * nS, a1 = (nS + nS) / v1;
        mpz_class &p1 = p, &q1 = q;
        p1 = nS * a1 + 1;
        q1 = a1;
        while (!isSolution(p1, q1)) {
            int u2 = a1 * v1 - u1;
            int v2 = (n - u2 * u2) / v1;
            int a2 = (nS + u2) / v2;
            mpz_class p2 = a2 * p1 + p0;
            mpz_class q2 = a2 * q1 + q0;
            p0 = p1, q0 = q1;
            u1 = u2, v1 = v2, a1 = a2, p1 = p2, q1 = q2;
        }
    }
};

int main() {
    mpz_class max = 0;
    int maxI = -1;
    for (int i = 1; i <= 1000; ++i) {
        PellEqn v(i);
        if (v.p <= max) continue;
        max = v.p;
        maxI = i;
    }
    printf("%d\n", maxI);
    return 0;
}
