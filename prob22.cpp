#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    auto in = fopen("data/prob22.txt", "r");
    vector<string> a;
    for (;;) {
        char s[80];
        if (fscanf(in, " \"%[^\"]\",", s) < 1) break;
        a.emplace_back(s);
    }
    fclose(in);
    sort(a.begin(), a.end());
    int sum = 0;
    for (int i = 0; i < (int)a.size(); ++i) {
        int s = 0;
        for (int j = 0; j < (int)a[i].size(); ++j)
            s += a[i][j] - 64;
        sum += s * (i + 1);
    }
    printf("%d\n", sum);
    return 0;
}
