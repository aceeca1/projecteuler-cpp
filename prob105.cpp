#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>
using namespace std;

struct SumSet {
    vector<int> a;

    SumSet(char* c) {
        for (auto p = strtok(c, ","); p; p = strtok(nullptr, ","))
            a.emplace_back(strtol(p, nullptr, 10));
    }

    bool is(int n, int b, int z, int zo) {
        if (n == (int)a.size())
            return !zo || (b && !(z < 0 && b >= 0) && !(z > 0 && b <= 0));
        if (!is(n + 1, b + a[n], z + 1, zo + 1)) return false;
        if (zo && !is(n + 1, b - a[n], z - 1, zo + 1)) return false;
        return is(n + 1, b, z, zo);
    }

    int sum() {
        int ans = 0;
        for (int i = 0; i < (int)a.size(); ++i) ans += a[i];
        return ans;
    }
};

int main() {
    char s[81];
    int ans = 0;
    while (scanf("%s", s) == 1) {
        SumSet a(s);
        if (a.is(0, 0, 0, 0)) ans += a.sum();
    }
    printf("%d\n", ans);
    return 0;
}
