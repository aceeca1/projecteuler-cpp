#include <cstdio>
#include <string>
#include "lib/Pandigital.h"
using namespace std;

int main() {
    string max = "0";
    for (int i = 1; i <= 9999; ++i) {
        char s[10];
        sprintf(s, "%d", i);
        string ss(s);
        for (int j = i + i; j <= i * 9; j += i) {
            sprintf(s, "%d", j);
            ss += s;
            if (ss.size() > 9) break;
            if (ss.size() < 9) continue;
            int c[10]{};
            for (char k: ss) ++c[k - '0'];
            if (!Pandigital::is(c)) continue;
            if (ss > max) max = ss;
        }
    }
    printf("%s\n", max.c_str());
    return 0;
}
