#include <cstdio>
#include <cstring>
using namespace std;

int pron(int n) {
    switch (n) {
        case 1: return strlen("one");
        case 2: return strlen("two");
        case 3: return strlen("three");
        case 4: return strlen("four");
        case 5: return strlen("five");
        case 6: return strlen("six");
        case 7: return strlen("seven");
        case 8: return strlen("eight");
        case 9: return strlen("nine");
        case 10: return strlen("ten");
        case 11: return strlen("eleven");
        case 12: return strlen("twelve");
        case 13: return strlen("thirteen");
        case 14: return strlen("fourteen");
        case 15: return strlen("fifteen");
        case 16: return strlen("sixteen");
        case 17: return strlen("seventeen");
        case 18: return strlen("eighteen");
        case 19: return strlen("nineteen");
        case 20: return strlen("twenty");
        case 30: return strlen("thirty");
        case 40: return strlen("forty");
        case 50: return strlen("fifty");
        case 60: return strlen("sixty");
        case 70: return strlen("seventy");
        case 80: return strlen("eighty");
        case 90: return strlen("ninety");
        case 1000: return strlen("oneThousand");
    }
    if (n < 100) return pron(n / 10 * 10) + pron(n % 10);
    if (!(n % 100)) return pron(n / 100) + strlen("hundred");
    return pron(n / 100 * 100) + strlen("and") + pron(n % 100);
}

int main() {
    int sum = 0;
    for (int i = 1; i <= 1000; ++i) sum += pron(i);
    printf("%d\n", sum);
    return 0;
}
