#include <cstdio>
#include <vector>
#include <climits>
#include "lib/Binary.h"
using namespace std;

struct Sudoku {
    static vector<vector<int>>& getE() {
        static vector<vector<int>> e(81);
        if (e[0].size()) return e;
        struct E {
            vector<int> k;
            void put() {
                if (k.size() == 8) {
                    if (!(
                        (k[0] == k[4] && k[1] == k[5]) ||
                        (k[2] == k[6] && k[3] == k[7]) ||
                        (k[0] == k[4] && k[2] == k[6]))) return;
                    int e0 = k[0] * 27 + k[1] * 9 + k[2] * 3 + k[3];
                    int e4 = k[4] * 27 + k[5] * 9 + k[6] * 3 + k[7];
                    if (e0 == e4) return;
                    e[e0].emplace_back(e4);
                    return;
                }
                for (int i = 0; i < 3; ++i) {
                    k.emplace_back(i);
                    put();
                    k.pop_back();
                }
            }
        };
        E().put();
        return e;
    }

    static void let(vector<int>& v, int pos, int val) {
        v[pos] = val;
        for (int i: getE()[pos]) {
            int &vi = v[i];
            bool b = vi == (vi & -vi);
            vi &= ~val;
            if (!vi) return;
            if (!b && vi == (vi & -vi)) let(v, i, vi);
        }
    }

    static void solve(const vector<int>& v) {
        int min = INT_MAX, minI = -1;
        for (int i = 0; i < 81; ++i) {
            int c = Binary::count14(v[i]);
            if (c == 1 || c >= min) continue;
            min = c;
            minI = i;
        }
        if (min == INT_MAX) throw v;
        int s = v[minI];
        for (;;) {
            int i = s & -s;
            if (!i) break;
            vector<int> v1(v);
            let(v1, minI, i);
            solve(v1);
            s -= i;
        }
    }
};


int main() {
    int sum = 0;
    auto in = fopen("data/prob96.txt", "r");
    for (;;) {
        if (fscanf(in, " Grid%*d") == EOF) break;
        vector<int> v;
        char s[10];
        for (int i = 0; i < 9; ++i) {
            fscanf(in, "%s", s);
            for (int j = 0; j < 9; ++j) {
                int p = s[j] - '0';
                v.emplace_back(p ? 1 << p : (1 << 10) - 2);
            }
        }
        for (int i = 0; i < 81; ++i) {
            if (v[i] != (v[i] & -v[i])) continue;
            Sudoku::let(v, i, v[i]);
        }
        try { Sudoku::solve(v); }
        catch (vector<int>& v){
            int w = 0;
            for (int i = 0; i < 3; ++i) {
                w = w * 10 + Binary::which(v[i]);
            }
            sum += w;
        }
    }
    fclose(in);
    printf("%d\n", sum);
    return 0;
}
