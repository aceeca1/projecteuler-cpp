#include <cstdio>
#include "lib/Prime.h"
using namespace std;

int main() {
    Prime p;
    for (int i = 1; i <= 10000; ++i) ++p;
    printf("%d\n", *p);
    return 0;
}
