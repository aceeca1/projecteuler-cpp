#include <cstdio>
#include <vector>
#include <gmpxx.h>
using namespace std;

int main() {
    vector<int> a{2};
    for (int i = 2; ; i += 2) {
        a.emplace_back(1);
        a.emplace_back(i);
        a.emplace_back(1);
        if (a.size() >= 100) break;
    }
    mpz_class num = 1, denom = 0;
    for (int i = 99; i >= 0; --i) {
        mpz_class num0(num);
        num *= a[i];
        num += denom;
        denom = num0;
    }
    int sum = 0;
    for (char i: num.get_str()) sum += i - '0';
    printf("%d\n", sum);
    return 0;
}
