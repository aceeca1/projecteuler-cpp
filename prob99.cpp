#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    double max = 0;
    int maxI = -1;
    auto in = fopen("data/prob99.txt", "r");
    for (int i = 1; ; ++i) {
        int b, e;
        if (fscanf(in, "%d,%d", &b, &e) < 2) break;
        double v = log(b) * e;
        if (v <= max) continue;
        max = v;
        maxI = i;
    }
    fclose(in);
    printf("%d\n", maxI);
    return 0;
}
