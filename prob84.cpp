#include <cstdio>
#include <vector>
#include <algorithm>
#include "lib/Matrix.h"
using namespace std;

int main() {
    vector<double> a(40);
    for (int i = 1; i <= 4; ++i)
        for (int j = 1; j <= 4; ++j)
            a[i + j] += 1.0 / 16;
    double threeDoubleJail = 3.0 / 1024;
    for (int i = 2; i <= 8; i += 2)
        a[i] -= threeDoubleJail;
    Matrix<double> b(40, 40);
    for (int i = 0; i < 40; ++i)
        for (int j = 0; j < 40; ++j) {
            int ij = j - i;
            if (ij < 0) ij += 40;
            b.a[i][j] = a[ij];
            if (j == 10) b.a[i][j] += threeDoubleJail * 4.0;
        }
    struct KV {
        int k;
        vector<int> v;
    };
    KV sp[]{
        {30, vector<int>(16, 10)},
        {7,  {0, 10, 11, 24, 39, 5, 15, 15, 12, 4 }},
        {22, {0, 10, 11, 24, 39, 5, 25, 25, 28, 19}},
        {36, {0, 10, 11, 24, 39, 5, 5,  5,  12, 33}},
        {2,  {0, 10}},
        {17, {0, 10}},
        {33, {0, 10}},
    };
    for (auto& kv: sp) {
        for (int i = 0; i < 40; ++i) {
            double share = b.a[i][kv.k] * (1.0 / 16);
            b.a[i][kv.k] = share * (16 - kv.v.size());
            for (int j: kv.v) b.a[i][j] += share;
        }
    }
    for (int i = 1; i <= 30; ++i) b = b * b;
    vector<int> c(40);
    for (int i = 0; i < 40; ++i) c[i] = i;
    sort(c.begin(), c.end(), [&](int x1, int x2) {
        return b.a[0][x1] > b.a[0][x2];
    });
    printf("%d%d%d\n", c[0], c[1], c[2]);
    return 0;
}
