#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

uint64_t choose(int n, int k) {
    uint64_t ret = 1;
    for (int i = 1; i <= k; ++i)
        ret = ret * (n + 1 - i) / i;
    return ret;
}

int main() {
    printf("%" PRId64 "\n", choose(40, 20));
    return 0;
}
