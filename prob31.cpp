#include <cstdio>
#include <initializer_list>
#include <vector>
using namespace std;

int main() {
    vector<int> a(201);
    a[0] = 1;
    for (int i: {1, 2, 5, 10, 20, 50, 100, 200})
        for (int j = i; j <= 200; ++j) a[j] += a[j - i];
    printf("%d\n", a[200]);
    return 0;
}
