#include <cstdio>
#include <vector>
#include "lib/Prime.h"
using namespace std;

struct PrimeSum {
    vector<int> a;

    PrimeSum(int n): a(n + 1) {
        a[0] = 1;
        for (Prime i; *i <= n; ++i)
            for (int j = *i; j <= n; ++j)
                a[j] += a[j - *i];
    }
};

int main() {
    int n = 2, n0 = 1;
    for (;;) {
        PrimeSum ps(n);
        for (int i = n0 + 1; i <= n; ++i)
            if (ps.a[i] >= 5000) {
                printf("%d\n", i);
                return 0;
            }
        n0 = n;
        n <<= 1;
    }
    return 0;
}
