#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include "lib/Prime.h"
using namespace std;

int main() {
    for (Prime i; ; ++i) {
        char s[10];
        sprintf(s, "%d", *i);
        for (char j: "012") {
            if (!strchr(s, j)) continue;
            vector<string> v{""};
            for (int k = 0; s[k]; ++k) {
                if (s[k] == j) {
                    for (int vi = v.size() - 1; vi >= 0; --vi) {
                        v.emplace_back(v[vi] + '*');
                        v[vi] += j;
                    }
                } else for (string& vi: v) vi += s[k];
            }
            v[0] = v.back();
            v.pop_back();
            for (string vi: v) {
                int num = 0;
                for (char c = j + 1; c <= '9'; ++c) {
                    string vic(vi);
                    for (int k = 0; k < (int)vic.size(); ++k)
                        if (vic[k] == '*') vic[k] = c;
                    int vicI;
                    sscanf(vic.c_str(), "%d", &vicI);
                    if (Prime::is(vicI)) ++num;
                }
                if (num < 7) continue;
                printf("%d\n", *i);
                return 0;
            }
        }
    }
    return 0;
}
