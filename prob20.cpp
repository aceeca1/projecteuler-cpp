#include <cstdio>
#include <string>
#include <gmpxx.h>
using namespace std;

int main() {
    mpz_class a(2);
    for (int i = 3; i <= 100; ++i) a *= i;
    int sum = 0;
    for (char i: a.get_str()) sum += i - '0';
    printf("%d\n", sum);
    return 0;
}
