#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

struct Poker {
    static int* getRank() {
        static int rank[256];
        if (rank['2']) return rank;
        for (int i = 0; i < 13; ++i) {
            rank[(int)"23456789TJQKA"[i]] = i;
        }
        return rank;
    }

    static string order(string* p) {
        int *rank = getRank();
        bool fl = all_of(p, p + 5, [&](const string& s) {
            return s[1] == p[0][1];
        });
        int a[13]{};
        for (int i = 0; i < 5; ++i) ++a[rank[(int)p[i][0]]];
        int k = 0; while (!a[k]) ++k;
        bool st = all_of(a + k, a + k + 5, [&](int n) {
            return n == 1;
        });
        if (fl && st) return string("9") += k + 'E';
        bool st0 = a[12] == 1 && all_of(a, a + 4, [&](int n) {
            return n == 1;
        });
        if (fl && st0) return "9D";
        vector<int> b[5];
        for (int i = 12; i >= 0; --i) {
            if (!a[i]) continue;
            b[a[i]].emplace_back(i);
        }
        string ret;
        if (b[4].size()) ret = "8";
        else if (b[3].size() && b[2].size()) ret = "7";
        else if (fl) ret = "6";
        else if (st) ret = "5";
        else if (st0) ret = "5D";
        else if (b[3].size()) ret = "4";
        else if (b[2].size() == 2) ret = "3";
        else if (b[2].size()) ret = "2";
        else ret = "1";
        for (int i = 4; i > 0; --i)
            for (int j: b[i]) ret += j + 'A';
        return ret;
    }
};

int main() {
    int count = 0;
    auto in = fopen("data/prob54.txt", "r");
    for (;;) {
        string s[10];
        for (int i = 0; i < 10; ++i) {
            char si[3];
            si[0] = 0;
            fscanf(in, "%s", si);
            s[i] = si;
        }
        if (!s[0].size()) break;
        string o1 = Poker::order(&s[0]);
        string o2 = Poker::order(&s[5]);
        if (o1 > o2) ++count;
    }
    fclose(in);
    printf("%d\n", count);
    return 0;
}
