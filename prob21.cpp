#include <cstdio>
#include "lib/Divisor.h"
using namespace std;

int main() {
    int sum = 0;
    for (int i = 2; i < 10000; ++i) {
        int k = Divisor::sum(i);
        if (k != i && Divisor::sum(k) == i) sum += i;
    }
    printf("%d\n", sum);
    return 0;
}
