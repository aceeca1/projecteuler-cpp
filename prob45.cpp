#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include "lib/Polygonal.h"
using namespace std;

int main() {
    for (int i = 144; ; ++i) {
        uint64_t n = Polygonal::num(6, i);
        if (!Polygonal::is(5, n)) continue;
        if (!Polygonal::is(3, n)) continue;
        printf("%" PRId64 "\n", n);
        return 0;
    }
    return 0;
}
