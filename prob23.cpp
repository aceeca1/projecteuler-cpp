#include <cstdio>
#include <bitset>
#include <vector>
#include "lib/Divisor.h"
using namespace std;

struct Abundant {
    bitset<28124> a;
    vector<int> b;

    Abundant() {
        for (int i = 2; i < 28124; ++i) {
            if (Divisor::sum(i) <= i) continue;
            a.set(i);
            b.emplace_back(i);
        }
    }

    bool twoSum(int n) const {
        for (int i = 0; i < (int)b.size(); ++i) {
            int k = n - b[i];
            if (k >= 0 && a[k]) return true;
        }
        return false;
    }
};

int main() {
    Abundant ab;
    int sum = 0;
    for (int i = 1; i < 28124; ++i) {
        if (ab.twoSum(i)) continue;
        sum += i;
    }
    printf("%d\n", sum);
    return 0;
}
