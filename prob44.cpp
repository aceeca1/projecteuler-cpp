#include <cstdio>
#include "lib/Polygonal.h"
#include "lib/Divisor.h"
using namespace std;

int main() try {
    for (int a = 1; ; ++a) {
        int ap = Polygonal::num(5, a), pq = ap + ap;
        Divisor(pq, [&](int p){
            int q = pq / p;
            int b = (p + 1 - 3 * q) / 6;
            if (b <= 0 || p != 3 * (b + b + q) -1) return;
            int bp = Polygonal::num(5, b);
            int cp = ap + bp;
            int dp = bp + cp;
            if (!Polygonal::is(5, dp)) return;
            throw ap;
        });
    }
} catch(int ap) {
    printf("%d\n", ap);
    return 0;
}
