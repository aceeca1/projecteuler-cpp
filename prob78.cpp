#include <cstdio>
#include <vector>
using namespace std;

struct CoinP {
    vector<int> a;

    CoinP(int n): a(n + 1) {
        a[0] = 1;
        for (int i = 1; i <= n; ++i)
            for (int j = i; j <= n; ++j)
                a[j] = (a[j] + a[j - i]) % 1000000;
    }
};

int main() {
    int n = 2, n0 = 1;
    for (;;) {
        CoinP cp(n);
        for (int i = n0 + 1; i <= n; ++i) {
            if (cp.a[i] == 0) {
                printf("%d\n", i);
                return 0;
            }
        }
        n0 = n;
        n <<= 1;
    }
    return 0;
}
