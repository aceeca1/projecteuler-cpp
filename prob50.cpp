#include <cstdio>
#include <vector>
#include <bitset>
#include "lib/Prime.h"
using namespace std;

int main() {
    vector<int> a;
    bitset<1000001> b;
    for (Prime i; *i <= 1000000; ++i) {
        a.emplace_back(*i);
        b.set(*i);
    }
    int max = 0, maxI = -1;
    for (int i = 0; i < (int)a.size(); ++i) {
        if (a[i] * max >= 1000000) break;
        int k = a[i];
        for (int j = i + 1; j < (int)a.size(); ++j) {
            k += a[j];
            if (k >= 1000000) break;
            if (!b[k]) continue;
            int len = j - i + 1;
            if (len <= max) continue;
            max = len;
            maxI = k;
        }
    }
    printf("%d\n", maxI);
    return 0;
}
