#include <cstdio>
#include <climits>
#include <vector>
#include <algorithm>
using namespace std;

constexpr int n = 12;

struct SMSet {
    int ans = 0;
    vector<int> a1, a2;

    void put(int m) {
        if (m == n) {
            if (a1.size() != a2.size()) return;
            if (a1.size() <= 1) return;
            for (int i = 0; i < (int)a1.size(); ++i)
                if (a1[i] > a2[i]) { ++ans; return; }
            return;
        }
        put(m + 1);
        a1.emplace_back(m);
        put(m + 1);
        a1.pop_back();
        if (!a1.empty() || !a2.empty()) {
            a2.emplace_back(m);
            put(m + 1);
            a2.pop_back();
        }
    }
};

int main() {
    SMSet sm;
    sm.put(0);
    printf("%d\n", sm.ans);
    return 0;
}
