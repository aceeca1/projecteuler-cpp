#include <cstdio>
#include <unordered_map>
#include <gmpxx.h>
using namespace std;

int main() {
    unordered_map<int, int> a;
    for (int n = 1; ; ++n) {
        mpz_class nZ = n;
        if (((1 + n) * (1 + n + n) << 1) > 1500000) break;
        for (int s = n + 1; ; s += 2) {
            int m = s * (s + n) << 1;
            if (m > 1500000) break;
            if (gcd(n, mpz_class(s)) != 1) continue;
            for (int sk = m; sk <= 1500000; sk += m) {
                ++a[sk];
            }
        }
    }
    int count = 0;
    for (auto i: a) {
        if (i.second != 1) continue;
        ++count;
    }
    printf("%d\n", count);
    return 0;
}
