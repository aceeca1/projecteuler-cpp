#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    unordered_set<int> a{1, 10, 100, 1000, 10000, 100000, 1000000};
    int c = 1, prod = 1;
    for (int i = 1; c <= 1000000; ++i) {
        char s[10];
        sprintf(s, "%d", i);
        for (int j = 0; s[j]; ++j) {
            if (a.count(c++)) prod *= s[j] - '0';
        }
    }
    printf("%d\n", prod);
    return 0;
}
