#include <cstdio>
#include <cmath>
#include <vector>
#include "lib/TortoiseHare.h"
using namespace std;

int main() {
    int count = 0;
    for (int i = 1; i <= 10000; ++i) {
        int iS = sqrt(i);
        if (iS * iS == i) continue;
        vector<int> a{0, 0, 1};
        if (!(TortoiseHare::len<vector<int>>(a, [&](vector<int>& ai) {
            ai[2] = (i - ai[1] * ai[1]) / ai[2];
            ai[0] = (iS - ai[1]) / ai[2];
            ai[1] = - ai[0] * ai[2] - ai[1];
        }) & 1)) continue;
        ++count;
    }
    printf("%d\n", count);
    return 0;
}
