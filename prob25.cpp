#include <cstdio>
#include <gmpxx.h>
#include "lib/Fibonacci.h"
using namespace std;

int main() {
    int count = 1;
    for (Fibonacci<mpz_class> i; i->get_str().size() < 1000; ++i) ++count;
    printf("%d\n", count);
    return 0;
}
