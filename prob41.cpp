#include <cstdio>
#include <algorithm>
#include <iterator>
#include "lib/Prime.h"
using namespace std;

int main() {
    int a[] = { 1, 2, 3, 4, 5, 6, 7 };
    for (; ; prev_permutation(begin(a), end(a))) {
        int b = 0;
        for (int i: a) b = b * 10 + i;
        if (!Prime::is(b)) continue;
        printf("%d\n", b);
        return 0;
    }
    return 0;
}
