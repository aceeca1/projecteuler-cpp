#include <cstdio>
#include "lib/Palindrome.h"
using namespace std;

int main() {
    int sum = 0;
    for (Palindrome i; ; ++i) {
        auto n = *i;
        if (n >= 1000000) break;
        if (!Palindrome::is<2>(n)) continue;
        sum += n;
    }
    printf("%d\n", sum);
    return 0;
}
