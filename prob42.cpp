#include <cstdio>
#include "lib/Polygonal.h"
using namespace std;

int main() {
    auto in = fopen("data/prob42.txt", "r");
    int count = 0;
    for (;;) {
        char s[80];
        if (fscanf(in, " \"%[^\"]\",", s) < 1) break;
        int sum = 0;
        for (int i = 0; s[i]; ++i) sum += s[i] - 64;
        if (Polygonal::is(3, sum)) ++count;
    }
    printf("%d\n", count);
    fclose(in);
    return 0;
}
