#include <cstdio>
#include <algorithm>
#include <iterator>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    uint64_t sum = 0;
    int a[] = { 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    while (next_permutation(begin(a), end(a))) {
        if (a[3] & 1) continue;
        if (a[5] % 5) continue;
        if ((a[7] * 100 + a[8] * 10 + a[9]) % 17) continue;
        if ((a[6] * 100 + a[7] * 10 + a[8]) % 13) continue;
        if ((a[5] * 100 + a[6] * 10 + a[7]) % 11) continue;
        if ((a[4] * 100 + a[5] * 10 + a[6]) % 7) continue;
        if ((a[2] + a[3] + a[4]) % 3) continue;
        uint64_t b = 0;
        for (int i: a) b = b * 10 + i;
        sum += b;
    }
    printf("%" PRId64 "\n", sum);
    return 0;
}
