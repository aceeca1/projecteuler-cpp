#ifndef FACTORIAL_H
#define FACTORIAL_H

struct Factorial {
    static int* get() {
        static int fac[10] = { 1, 1 };
        if (fac[9]) return fac;
        for (int i = 2; i <= 9; ++i) fac[i] = fac[i - 1] * i;
        return fac;
    }
};

#endif
