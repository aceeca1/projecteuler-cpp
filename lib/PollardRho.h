#ifndef POLLARD_RHO_H
#define POLLARD_RHO_H

#include <vector>
#include <gmpxx.h>
using namespace std;

struct PollardRho {
    static bool isPrime(const mpz_class& n) {
        return mpz_probab_prime_p(n.get_mpz_t(), 15);
    }

    static size_t bitLength(const mpz_class& n) {
        return mpz_sizeinbase(n.get_mpz_t(), 2);
    }

    struct SomeDivisor {
        mpz_class c, n;
        SomeDivisor(const mpz_class& n_): n(n_) {}
        void f(mpz_class& k) {
            k *= k; k += c; k %= n;
        }
        mpz_class operator()() {
            static gmp_randclass rand(gmp_randinit_default);
            auto h = n;
            while (h == n) {
                c = rand.get_z_bits(bitLength(n));
                auto r = rand.get_z_bits(bitLength(n));
                int n1 = 0, n2 = 0;
                mpz_class x1 = r, x2 = r; f(x2);
                h = 1;
                while (h == 1) {
                    if (n2 > n1 + n1) { n1 = n2; x1 = x2; }
                    ++n2; f(x2);
                    h = gcd(x1 - x2, n);
                }
            }
            return h;
        }
    };

    static void factorize(vector<mpz_class>& a, const mpz_class& n) {
        if (n == 1) return;
        if (isPrime(n)) { a.emplace_back(n); return; }
        auto d = SomeDivisor(n)();
        factorize(a, d);
        factorize(a, n / d);
    }

    static auto factorize(mpz_class n) -> vector<mpz_class> {
        vector<mpz_class> a;
        factorize(a, n);
        return a;
    }
};

#endif
