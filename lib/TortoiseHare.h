#ifndef TORTOISE_HARE_H
#define TORTOISE_HARE_H

#include <functional>
using namespace std;

struct TortoiseHare {
    template <class T>
    static int len(const T& n, function<void(T&)> f) {
        int n1 = 0, n2 = 1;
        T t1 = n, t2 = n; f(t2);
        while (t2 != t1) {
            if (n2 > n1 + n1) { n1 = n2; t1 = t2; }
            ++n2; f(t2);
        }
        return n2 - n1;
    }
};
#endif
