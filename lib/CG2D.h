#ifndef CG2D
#define CG2D

#include <complex>
#include <vector>
using namespace std;

typedef complex<double> Point;

double cross(const Point& p1, const Point& p2) {
    return p1.real() * p2.imag() - p1.imag() * p2.real();
}

double dot(const Point& p1, const Point& p2) {
    return p1.real() * p2.real() + p1.imag() * p2.imag();
}

struct Segment {
    Point c1, c2;
};

void intersectPoint(const Segment& s1, const Segment& s2, Point& p) {
    Point e = s1.c2 - s1.c1;
    double a1 = cross(e, s2.c1 - s1.c1);
    double a2 = cross(e, s2.c2 - s1.c1);
    if (a1 * a2 > -1e-12) return;
    e = (s2.c1 * a2 - s2.c2 * a1) / (a2 - a1);
    if (dot(s1.c1 - e, s1.c2 - e) < -1e-12) p = e;
}

bool isInside(const Point& p, const vector<Point>& a) {
    for (int i = 0; i < (int)a.size() - 1; ++i)
        if (cross(a[i] - p, a[i + 1] - p) < 1e-12) return false;
    return true;
}

bool onSeg(const Point& p, const Segment& s) {
    Point v1 = s.c1 - p, v2 = s.c2 - p;
    double c = cross(v1, v2);
    return -1e-12 < c && c < 1e-12 && dot(v1, v2) < -1e-12;
}

#endif
