#ifndef MAX_PATH_SUM_H
#define MAX_PATH_SUM_H

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct MaxPathSum {
    int n;
    const char *fileName;

    int operator()() const {
        auto in = fopen(fileName, "r");
        vector<vector<int>> a(n);
        for (int i = 0; i < n; ++i) {
            a[i].resize(i + 1);
            for (int j = 0; j <= i; ++j)
                fscanf(in, "%d", &a[i][j]);
        }
        fclose(in);
        for (int i = n - 2; i >= 0; --i)
            for (int j = 0; j <= i; ++j)
                a[i][j] += max(a[i + 1][j], a[i + 1][j + 1]);
        return a[0][0];
    }
};

#endif
