#ifndef FIBONACCI_H
#define FIBONACCI_H

template <class T>
struct Fibonacci {
    T a1 = 1, a2 = 1;
    Fibonacci& operator++() {
        T a3 = a1 + a2;
        a1 = a2;
        a2 = a3;
        return *this;
    }
    const T& operator*() const {return a1;}
    const T* operator->() const {return &a1;}
};


#endif
