#ifndef HASH_H
#define HASH_H

#include <cstdint>
using namespace std;

struct Hash {
    static uint64_t h(int n) {
        const uint64_t kMul = 0x9ddfea08eb382d69ULL;
        uint64_t nL = n * kMul;
        nL ^= nL >> 47;
        nL *= kMul;
        return nL ^= nL >> 47;
    }
};

#endif
