#ifndef DIVISOR_H
#define DIVISOR_H

#include <vector>
#include <functional>
#include <unordered_map>
#include "Prime.h"
using namespace std;

struct Divisor {
    static int sum(int n) {
        auto a = Prime::factorize(n);
        a.emplace_back(0);
        int ret = 1, k = a[0] + 1;
        for (int i = 1; i < (int)a.size(); ++i)
            if (a[i] != a[i - 1]) { ret *= k; k = a[i] + 1; }
            else k = k * a[i] + 1;
        return ret - n;
    }

    vector<int> k, v;
    typedef function<void(int)> func;
    func f;

    void foreach(int p, int u) {
        if (p >= (int)k.size()) { f(u); return; }
        for (int i = 0; i <= v[p]; ++i) {
            foreach(p + 1, u);
            u *= k[p];
        }
    }

    Divisor(int n, func f_): f(f_) {
        unordered_map<int, int> h;
        for (int i: Prime::factorize(n)) ++h[i];
        for (auto i = h.begin(); i != h.end(); ++i) {
            k.emplace_back(i->first);
            v.emplace_back(i->second);
        }
        foreach(0, 1);
    }
};

#endif
