#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
using namespace std;

template <class T>
struct Matrix {
    vector<vector<T>> a;

    Matrix(int p1, int p2): a(p1, vector<T>(p2)) {}

    Matrix operator*(const Matrix& rhs) const {
        Matrix ret(a.size(), rhs.a[0].size());
        for (int i = 0; i < (int)a.size(); ++i)
            for (int j = 0; j < (int)a[0].size(); ++j)
                for (int k = 0; k < (int)rhs.a[0].size(); ++k)
                    ret.a[i][k] += a[i][j] * rhs.a[j][k];
        return ret;
    }
};

#endif
