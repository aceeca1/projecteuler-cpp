#ifndef PHI_H
#define PHI_H

#include <vector>
#include "Prime.h"
using namespace std;

struct Phi {
    vector<int> a;

    Phi(int n) {
        a.resize(n + 1);
        a[1] = 1;
        for (int i = 2; i <= n; ++i) {
            int minF = Prime::minFactor(i);
            int b = i / minF;
            if (b % minF)
                a[i] = a[b] * (minF - 1);
            else
                a[i] = a[b] * minF;
        }
    }
};

#endif
