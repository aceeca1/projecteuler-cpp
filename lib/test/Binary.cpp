#include <cstdio>
#include "../Binary.h"
using namespace std;

int main() {
    printf("%s\n", Binary::str(1000).c_str());
    printf("%s\n", Binary::str(1024).c_str());
    return 0;
}
