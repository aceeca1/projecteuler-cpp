#include <cstdio>
#include "../Prime.h"
using namespace std;

int main() {
    for (Prime i; *i <= 100000; ++i) {
        printf("%d\n", *i);
    }
    for (int i = 1; i <= 100000; ++i) {
        for (int j : Prime::factorize(i))
            printf("%d ", j);
        printf("\n");
    }
    return 0;
}
