#include <cstdio>
#include "../PollardRho.h"
using namespace std;

int main() {
    for (int i = 2; i <= 100; ++i) {
        printf("%d: %d\n", i, (int)PollardRho::isPrime(i));
        for (auto& j : PollardRho::factorize(i)) {
            gmp_printf("%Zd ", j.get_mpz_t());
        }
        printf("\n");
    }
    return 0;
}
