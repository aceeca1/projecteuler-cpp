#ifndef MIN_SPAN_TREE
#define MIN_SPAN_TREE

#include <climits>
#include <vector>
using namespace std;

struct MSTVertex {
    vector<int> e;
    int d;
};

struct MSTGraph {
    vector<MSTVertex> v;

    int prim() {
        int ans = 0;
        for (int i = 0; i < (int)v.size(); ++i) v[i].d = INT_MAX;
        v[0].d = 0;
        for (int i = 0; i < (int)v.size(); ++i) {
            int dMin = INT_MAX, dMinNo = -1;
            for (int j = 0; j < (int)v.size(); ++j)
                if (v[j].d != -1 && v[j].d < dMin) {
                    dMin = v[j].d;
                    dMinNo = j;
                }
            ans += dMin;
            v[dMinNo].d = -1;
            for (int j = 0; j < (int)v.size(); ++j)
                if (v[dMinNo].e[j] != -1 && v[dMinNo].e[j] < v[j].d)
                    v[j].d = v[dMinNo].e[j];
        }
        return ans;
    }
};

#endif
