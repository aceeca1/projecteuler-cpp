#ifndef SHORT_PATH_H
#define SHORT_PATH_H

#include <vector>
#include <climits>
#include <deque>
using namespace std;

struct SPEdge {
    int t, c;
};

struct SPVertex {
    vector<SPEdge> e;
    int d;
    bool q;
};

struct SPGraph {
    vector<SPVertex> v;

    int addVertex() {
        v.emplace_back(SPVertex{{}, INT_MAX, 0});
        return v.size() - 1;
    }

    void addEdge(int s, int t, int c) {
        v[s].e.emplace_back(SPEdge{t, c});
    }

    void shortPath(int s) {
        v[s].d = 0;
        v[s].q = true;
        deque<int> dq{s};
        while (!dq.empty()) {
            int n = dq.front();
            dq.pop_front();
            v[n].q = false;
            for (SPEdge& j: v[n].e) {
                int dist = v[n].d + j.c;
                if (dist >= v[j.t].d) continue;
                v[j.t].d = dist;
                if (v[j.t].q) continue;
                v[j.t].q = true;
                if (!dq.empty() && v[j.t].d < v[dq.front()].d)
                    dq.emplace_front(j.t);
                else dq.emplace_back(j.t);
            }
        }
    }
};

#endif
