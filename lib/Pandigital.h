#ifndef PANDIGITAL_H
#define PANDIGITAL_H

struct Pandigital {
    static bool is(int* c) {
        if (c[0]) return false;
        for (int i = 1; i <= 9; ++i)
            if (c[i] != 1) return false;
        return true;
    }
};

#endif
