#ifndef POLYGONAL_H
#define POLYGONAL_H

#include <cstdint>
#include <cmath>
using namespace std;

struct Polygonal {
    static uint64_t num(int k, uint64_t n) {
        return (n * (n - 1) >> 1) * (k - 2) + n;
    }

    static bool is(int k, uint64_t n) {
        double nD = n;
        switch (k) {
            case 3: return n == num(3, (uint64_t)sqrt(nD + nD));
            case 4: return n == num(4, (uint64_t)sqrt(nD));
        }
        return n == num(k, (uint64_t)sqrt((nD + nD) / (k - 2)) + 1);
    }
};

#endif
