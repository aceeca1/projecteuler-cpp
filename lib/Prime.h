#ifndef PRIME_H
#define PRIME_H

#include <vector>
using namespace std;

struct Prime {
    static int* getP() {
        static int p[65536];
        if (p[0]) return p;
        int u = 0;
        for (int i = 2; i < 65536; ++i) {
            int v = p[i];
            if (!v) u = p[u] = v = i;
            for (int w = 2; i * w < 65536; w = p[w]) {
                p[i * w] = w;
                if (w >= v) break;
            }
        }
        p[u] = 65536;
        return p;
    }

    static bool is(int n) {
        int *p = getP();
        if (n < 65536) return p[n] > n;
        for (int i = 2; i * i <= n; i = p[i])
            if (n % i == 0) return false;
        return true;
    }

    static int minFactor(int n) {
        int *p = getP();
        if (n < 65536) return p[n] < n ? p[n] : n;
        for (int i = 2; i * i <= n; i = p[i])
            if (!(n % i)) return i;
        return n;
    }

    static vector<int> factorize(int n) {
        vector<int> a;
        int *p = getP();
        for (int i = 2; n >= 65536; )
            if (i * i > n) {
                a.emplace_back(n);
                return a;
            } else if (!(n % i)) {
                n /= i;
                a.emplace_back(i);
            } else i = p[i];
        if (n == 1) return a;
        while (p[n] < n) {
            a.emplace_back(p[n]);
            n /= p[n];
        }
        a.emplace_back(n);
        return a;
    }

    int i = 2;
    int operator*() const { return i; }
    Prime& operator++() {
        if (i < 65521) {
            i = getP()[i];
            return *this;
        }
        i += 2;
        while (!is(i)) i += 2;
        return *this;
    }
};

#endif
