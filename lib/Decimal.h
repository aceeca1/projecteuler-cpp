#ifndef DECIMAL_H
#define DECIMAL_H

struct Decimal {
    static int* shl() {
        static int dec[10]{1};
        if (dec[9]) return dec;
        for (int i = 1; i < 10; ++i) dec[i] = dec[i - 1] * 10;
        return dec;
    }
};

#endif
