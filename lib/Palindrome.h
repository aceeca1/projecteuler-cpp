#ifndef PALINDROME_H
#define PALINDROME_H

#include <cstdio>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include "Binary.h"
using namespace std;

struct Palindrome {
    template <int base>
    static string str(int n) {
        if (base == 2) return Binary::str(n);
        const char *fmt = nullptr;
        if (base == 8) fmt = "%o";
        if (base == 10) fmt = "%d";
        if (base == 16) fmt = "%x";
        if (!fmt) throw invalid_argument("");
        char s[13];
        sprintf(s, fmt, n);
        return s;
    }

    template <int base>
    static bool is(int n) {
        auto s = str<base>(n);
        return equal(s.begin(), s.end(), s.rbegin());
    }

    int z = 1;
    vector<int> a = {0};

    Palindrome& operator++() {
        int i = 0;
        while (i < (int)a.size() && a[i] == 9) a[i++] = 0;
        if (i != (int)a.size()) ++a[i];
        else if (z++ & 1) a[a.size() - 1] = 1;
        else a.emplace_back(1);
        return *this;
    }

    int operator*() const {
        int ret = 0;
        for (int i = a.size() - 1; i >= 0; --i) ret = ret * 10 + a[i];
        for (int i = z & 1; i < (int)a.size(); ++i) ret = ret * 10 + a[i];
        return ret;
    }
};

#endif
