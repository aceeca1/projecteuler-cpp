#ifndef BINARY_H
#define BINARY_H

#include <cstdio>
#include <string>
#include <cstdint>
using namespace std;

struct Binary {
    static string str(int n) {
        static const char *b[] = {
            "000", "001", "010", "011", "100", "101", "110", "111"
        };
        char s[13];
        sprintf(s, "%o", n);
        string ret(b[s[0] - '0']);
        int k = 0; while (k < 2 && ret[k] == '0') ++k;
        ret.erase(0, k);
        for (int i = 1; s[i]; ++i) ret += b[s[i] - '0'];
        return ret;
    }

    static int count14(uint32_t n) {
        return (n * 0x200040008001ULL & 0x111111111111111ULL) % 0xf;
    }

    static int which(uint32_t n) {
        static const int deBruijn[32] {
            0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
            31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
        };
        return deBruijn[n * 0x077CB531U >> 27];
    }
};

#endif
