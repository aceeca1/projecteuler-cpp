#include <cstdio>
#include <gmpxx.h>
#include "lib/PollardRho.h"
using namespace std;

int main() {
    auto a = PollardRho::factorize(600851475143);
    gmp_printf("%Zd\n", max_element(a.begin(), a.end())->get_mpz_t());
    return 0;
}
