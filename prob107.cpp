#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "lib/MinSpanTree.h"
using namespace std;

constexpr int M = 40;

int parse(char* s, vector<int>& e) {
    int z = 0, ans = 0;
    for (auto p = strtok(s, ","); p; p = strtok(nullptr, ",")) {
        char* q;
        int a = strtol(p, &q, 10);
        if (p == q) a = -1;
        else ans += a;
        e[z++] = a;
    }
    return ans;
}

int main() {
    MSTGraph g;
    g.v.resize(M);
    int total = 0;
    for (int i = 0; i < M; ++i) {
        g.v[i].e.resize(M);
        char s[161];
        scanf("%s", s);
        total += parse(s, g.v[i].e);
    }
    printf("%d\n", (total >> 1) - g.prim());
    return 0;
}
