#include <cstdio>
#include <vector>
#include <climits>
using namespace std;

int main() {
    vector<vector<int>> a(80, vector<int>(80));
    auto in = fopen("data/prob81.txt", "r");
    for (int i = 0; i < 80; ++i)
        for (int j = 0; j < 80; ++j) fscanf(in, "%d,", &a[i][j]);
    fclose(in);
    vector<int> b(80);
    for (int i = 1; i < 80; ++i) b[i] = INT_MAX;
    for (auto& ai: a) {
        b[0] += ai[0];
        for (int j = 1; j < 80; ++j) {
            if (b[j - 1] < b[j]) b[j] = b[j - 1];
            b[j] += ai[j];
        }
    }
    printf("%d\n", b[79]);
    return 0;
}
