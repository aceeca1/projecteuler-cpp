#include <cstdio>
#include <vector>
#include <algorithm>
#include <iterator>
#include "lib/Polygonal.h"
using namespace std;

struct CycFig {
    vector<int> a[6];
    int b[5] = { 0, 1, 2, 3, 4 }, c[6];

    CycFig() {
        for (int i = 3; i <= 8; ++i)
            for (int j = 1000; j < 10000; ++j)
                if (Polygonal::is(i, j)) a[i - 3].emplace_back(j);
    }

    int search(int k) {
        switch (k) {
        case 0:
            for (;;) {
                int r = search(1);
                if (r) return r;
                next_permutation(begin(b), end(b));
            }; break;
        case 1:
            for (int i: a[5]) {
                c[0] = i;
                int r = search(2);
                if (r) return r;
            }; break;
        case 2: case 3: case 4: case 5: case 6:
            for (int i: a[b[k - 2]]) {
                if (i / 100 != c[k - 2] % 100) continue;
                c[k - 1] = i;
                int r = search(k + 1);
                if (r) return r;
            }; break;
        case 7:
            if (c[0] / 100 != c[5] % 100) return 0;
            return c[0] + c[1] + c[2] + c[3] + c[4] + c[5];
        }
        return 0;
    }
};

int main() {
    printf("%d\n", CycFig().search(0));
    return 0;
}
