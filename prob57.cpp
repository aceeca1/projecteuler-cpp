#include <cstdio>
#include <gmpxx.h>
using namespace std;

struct SqrtCov {
    mpz_class n1 = 3, n2 = 2;

    SqrtCov& operator++() {
        mpz_class n2o(n2);
        n2 += n1;
        n1 = n2o += n2;
        return *this;
    }

    bool numGtDenom() const {
        return n1.get_str().size() > n2.get_str().size();
    }
};

int main() {
    int count = 0;
    SqrtCov k;
    for (int i = 0; i < 1000; ++i) {
        count += k.numGtDenom();
        ++k;
    }
    printf("%d\n", count);
    return 0;
}
