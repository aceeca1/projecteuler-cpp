#include <cstdio>
#include <vector>
#include <climits>
#include <stack>
#include <algorithm>
using namespace std;

struct PSNum {
    int pSum, pProd, cSize, c;
};

int main() {
    vector<int> mps(12001, INT_MAX);
    stack<PSNum> s;
    s.emplace(PSNum{0, 1, 1, 2});
    while (!s.empty()) {
        PSNum si = s.top();
        s.pop();
        int cSum = si.pSum + si.c;
        int cProd = si.pProd * si.c;
        int fSize = si.cSize + (cProd - cSum);
        if (fSize > 12000) continue;
        if (si.cSize == 1 && si.c * si.c > 12000) continue;
        if (cProd < mps[fSize]) mps[fSize] = cProd;
        s.emplace(PSNum{cSum, cProd, si.cSize + 1, si.c});
        s.emplace(PSNum{si.pSum, si.pProd, si.cSize, si.c + 1});
    }
    sort(&mps[2], &mps[12001]);
    int mpsZ = unique(&mps[2], &mps[12001]) - &mps[0];
    int sum = 0;
    for (int i = 2; i < mpsZ; ++i) sum += mps[i];
    printf("%d\n", sum);
    return 0;
}
