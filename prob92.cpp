#include <cstdio>
#include <vector>
#include <stack>
using namespace std;

struct SSEndTo {
    vector<int> a;

    SSEndTo(): a(10000000) {
        a[1] = 1;
        a[89] = 89;
        for (int i = 1; i < 10000000; ++i) {
            if (a[i]) continue;
            stack<int> st;
            st.emplace(i);
            int k;
            for (;;) {
                k = st.top();
                if (a[k]) break;
                char s[10];
                sprintf(s, "%d", k);
                int sum = 0;
                for (int j = 0; s[j]; ++j) {
                    int sj = s[j] - '0';
                    sum += sj * sj;
                }
                st.emplace(sum);
            }
            while (!st.empty()) {
                int sti = st.top();
                st.pop();
                a[sti] = a[k];
            }
        }
    }
};

int main() {
    SSEndTo s;
    int count = 0;
    for (int i = 1; i < 10000000; ++i) {
        if (s.a[i] != 89) continue;
        ++count;
    }
    printf("%d\n", count);
    return 0;
}
