#include <cstdio>
#include <vector>
#include <unordered_set>
#include "lib/Hash.h"
using namespace std;

int main() {
    vector<unordered_set<uint64_t>> w(1001);
    for (int k = 1; k <= 500; ++k)
        for (int m = 1; k * m * m < 500; ++m)
            for (int n = 1; n < m && k * m * (m + n) <= 500; ++n) {
                int m2 = m * m, n2 = n * n;
                int a = k * (m2 - n2);
                int b = k * m * n << 1;
                int c = k * (m2 + n2);
                w[a + b + c].emplace(Hash::h(a) ^ Hash::h(b) ^ Hash::h(c));
            }
    int max = 0, maxI = -1;
    for (int i = 0; i <= 1000; ++i) {
        if ((int)w[i].size() <= max) continue;
        max = w[i].size();
        maxI = i;
    }
    printf("%d\n", maxI);
    return 0;
}
