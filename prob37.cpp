#include <cstdio>
#include <initializer_list>
#include "lib/Prime.h"
using namespace std;

int fillTPrime(int a) {
    int r = 0;
    for (int i: {1, 3, 7, 9}) {
        int ai = a * 10 + i;
        if (!Prime::is(ai)) continue;
        r += fillTPrime(ai);
    }
    for (int i = 10; i < a; i *= 10)
        if (!Prime::is(a % i)) return r;
    return r + a;
}

int main() {
    int sum = 0;
    for (int i: {2, 3, 5, 7}) sum += fillTPrime(i);
    printf("%d\n", sum - 2 - 3 - 5 - 7);
    return 0;
}
