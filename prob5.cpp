#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    mpz_class lcm = 1;
    for (int i = 2; i <= 20; ++i)
        lcm = i / gcd(lcm, i) * lcm;
    gmp_printf("%Zd\n", lcm.get_mpz_t());
    return 0;
}
