#include <cstdio>
using namespace std;

int main() {
    for (int m = 1; m <= 500; ++m) {
        if (500 % m) continue;
        int kp = 500 / m;
        for (int p = m + 1; p < m + m; ++p) {
            if (kp % p) continue;
            int k = kp / p, n = p - m;
            int m2 = m * m, n2 = n * n;
            int a = k * (m2 - n2), b = k * m * n << 1, c = k * (m2 + n2);
            printf("%d\n", a * b * c);
            return 0;
        }
    }
    return 0;
}
