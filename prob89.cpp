#include <cstdio>
#include <string>
#include <regex>
using namespace std;

int main() {
    const char* a[11][2] = {
        {"IIII", "IV"}, {"VV", "X"}, {"VIV", "IX"}, {"XXXXX", "L"},
        {"XXXX", "XL"}, {"LL", "C"}, {"LXL", "IC"}, {"CCCCC", "D"},
        {"CCCC", "CD"}, {"DD", "M"}, {"DCD", "IM"}};
    vector<regex> re;
    for (int i = 0; i < 11; ++i) re.emplace_back(a[i][0]);
    int sum = 0;
    auto in = fopen("data/prob89.txt", "r");
    for (;;) {
        char s[80];
        if (fscanf(in, "%s", s) < 1) break;
        string ss(s);
        int len = ss.size();
        for (int i = 0; i < 11; ++i)
            ss = regex_replace(ss, re[i], a[i][1]);
        sum += len - ss.size();
    }
    fclose(in);
    printf("%d\n", sum);
    return 0;
}
