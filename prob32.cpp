#include <cstdio>
#include <unordered_set>
#include <initializer_list>
#include "lib/Pandigital.h"
using namespace std;

struct PandigitalProd {
    unordered_set<int> a;

    void f(int n1, int n2) {
        for (int i = n1; i < n1 * 10; ++i)
            for (int j = n2; j < n2 * 10; ++j) {
                int k = i * j;
                if (k > 10000) break;
                int c[10]{};
                for (int p: {i, j, k}) {
                    char s[10];
                    sprintf(s, "%d", p);
                    for (int q = 0; s[q]; ++q) ++c[s[q] - '0'];
                }
                if (!Pandigital::is(c)) continue;
                a.emplace(k);
            }
    }

    PandigitalProd() {
        f(10, 100);
        f(1, 1000);
    }
};

int main() {
    int sum = 0;
    for (int i: PandigitalProd().a) sum += i;
    printf("%d\n", sum);
    return 0;
}
