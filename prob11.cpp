#include <cstdio>
using namespace std;

int main() {
    auto in = fopen("data/prob11.txt", "r");
    int a[20][20];
    for (int i = 0; i < 20; ++i)
        for (int j = 0; j < 20; ++j)
            fscanf(in, "%d", &a[i][j]);
    fclose(in);
    int max = 0;
    for (int i0 = 0; i0 < 20; ++i0)
        for (int j0 = 0; j0 < 20; ++j0) {
            int k[4][2] = { {1, 1}, {1, 0}, {0, 1}, {1, -1} };
            for (int ki = 0; ki < 4; ++ki) {
                int i1 = i0 + k[ki][0], j1 = j0 + k[ki][1];
                int i2 = i1 + k[ki][0], j2 = j1 + k[ki][1];
                int i3 = i2 + k[ki][0], j3 = j2 + k[ki][1];
                if (i3 >= 20 || j3 >= 20 || j3 < 0) continue;
                int b = a[i0][j0] * a[i1][j1] * a[i2][j2] * a[i3][j3];
                if (b > max) max = b;
            }
        }
    printf("%d\n", max);
    return 0;
}
