#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    int count = 0;
    for (int i = 1; i <= 50; ++i)
        for (int j = 1; j <= 50; ++j) {
            mpz_class dZ = gcd(mpz_class(i), j);
            int d = dZ.get_si();
            int k1 = 50 - i;
            int k2 = j * j / i;
            count += (k1 < k2 ? k1 : k2) / (j / d);
        }
    printf("%d\n", (count << 1) + 3 * 50 * 50);
}
