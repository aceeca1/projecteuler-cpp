#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int count = 0;
    vector<double> a{1.0};
    for (int i = 1; i <= 100; ++i) {
        double prev = 1.0;
        for (int j = 1; j < (int)a.size(); ++j) {
            double aj = a[j] + prev;
            prev = a[j];
            a[j] = aj;
            if (aj > 1e6) ++count;
        }
        a.emplace_back(1.0);
    }
    printf("%d\n", count);
    return 0;
}
