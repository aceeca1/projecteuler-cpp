#include <cstdio>
#include <queue>
#include "lib/Prime.h"
using namespace std;

struct TwoSSeq {
    int x1, x2, v;
    TwoSSeq(int x1_) :x1(x1_), v(x1_) {}

    TwoSSeq& operator++() {
        ++x2;
        v = x1 + (x2 * x2 << 1);
        return *this;
    }
    bool operator<(const TwoSSeq& rhs) const {
        return v > rhs.v;
    }
};

struct PrimeTwoSSeq {
    Prime p;
    priority_queue<TwoSSeq> q;
    PrimeTwoSSeq() { q.emplace(*p); }

    int operator*() const { return q.top().v; }
    PrimeTwoSSeq& operator++() {
        auto qi = q.top();
        q.pop();
        if (qi.x1 == *p) q.emplace(*++p);
        ++qi;
        q.emplace(move(qi));
        return *this;
    }
};


int main() {
    PrimeTwoSSeq a;
    for (int i = 3; ; i += 2) {
        while (*a < i) ++a;
        if (*a == i) continue;
        printf("%d\n", i);
        return 0;
    }
    return 0;
}
