#include <cstdio>
using namespace std;

int main() {
    int sum = 1, a = 1;
    for (int i = 2; i <= 1000; i += 2)
        for (int j = 0; j < 4; ++j) { a += i; sum += a; }
    printf("%d\n", sum);
    return 0;
}
