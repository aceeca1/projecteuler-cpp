#include <cstdio>
#include <algorithm>
#include "lib/Prime.h"
using namespace std;

int main() {
    int i = 2;
    for (int h = 0; h < 4; ++i) {
        auto a = Prime::factorize(i);
        if (unique(a.begin(), a.end()) - a.begin() == 4) ++h;
        else h = 0;
    }
    printf("%d\n", i - 4);
    return 0;
}
