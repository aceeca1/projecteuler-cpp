#include <cstdio>
using namespace std;

int main() {
    double max = 0;
    int maxNum = -1;
    for (int i = 1; i <= 1000000; ++i) {
        int num = (i * 3 - 1) / 7;
        double ratio = (double)num / i;
        if (ratio <= max) continue;
        max = ratio;
        maxNum = num;
    }
    printf("%d\n", maxNum);
    return 0;
}
