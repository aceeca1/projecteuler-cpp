#include <cstdio>
#include <string>
#include <vector>
#include <queue>
#include <unordered_set>
using namespace std;

struct Passcode {
    string s;
    vector<string> v;
    int prio;

    Passcode(string&& s_, vector<string>&& v_): s(s_), v(v_) {
        int h = 0;
        for (string& i: v) {
            if ((int)i.size() <= h) continue;
            h = i.size();
        }
        prio = s.size() + h;
    }

    bool operator<(const Passcode& rhs) const {
        return prio > rhs.prio;
    }
};

int main() {
    vector<string> v;
    auto in = fopen("data/prob79.txt", "r");
    for (;;) {
        char s[10];
        if (fscanf(in, "%s", s) < 1) break;
        v.emplace_back(s);
    }
    fclose(in);
    priority_queue<Passcode> pq;
    pq.emplace("", move(v));
    while (!pq.empty()) {
        auto n = pq.top();
        pq.pop();
        if (n.v.empty()) {
            printf("%s\n", n.s.c_str());
            return 0;
        }
        unordered_set<char> c;
        for (int i = 0; i < (int)n.v.size(); ++i) {
            c.emplace(n.v[i][0]);
        }
        for (char i: c) {
            vector<string> v1;
            for (string& j: n.v) {
                if (i != j[0]) v1.emplace_back(j);
                else if (j.size() > 1)
                    v1.emplace_back(j.substr(1, j.size() - 1));
            }
            pq.emplace(n.s + i, move(v1));
        }
    }
    return 0;
}
