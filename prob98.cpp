#include <cstdio>
#include <vector>
#include <string>
#include <cmath>
#include <cstring>
#include <unordered_map>
#include "lib/Hash.h"
#include "lib/Decimal.h"
using namespace std;

int main() {
    unordered_map<uint64_t, vector<string>> a;
    auto in = fopen("data/prob98.txt", "r");
    for (;;) {
        char s[81];
        if (fscanf(in, " \"%[^\"]\",", s) < 1) break;
        uint64_t hash = 0;
        for (int i = 0; s[i]; ++i) hash += Hash::h(s[i]);
        a[hash].emplace_back(s);
    }
    fclose(in);
    int max = 0;
    auto dec = Decimal::shl();
    for (auto i: a)
        for (int j1 = 0; j1 < (int)i.second.size(); ++j1)
            for (int j2 = j1 + 1; j2 < (int)i.second.size(); ++j2) {
                string &s1 = i.second[j1], &s2 = i.second[j2];
                int n = s1.size();
                int lB = sqrt(dec[n - 1]) + 0.5;
                int uB = sqrt(dec[n] - 1) + 0.5;
                for (int k1 = lB; k1 <= uB; ++k1) {
                    char x1[10];
                    sprintf(x1, "%d", k1 * k1);
                    if ((int)strlen(x1) != n) continue;
                    bool ooMap = true;
                    unordered_map<char, char> tr;
                    for (int i = 0; i < n; ++i) tr[x1[i]] = s1[i];
                    for (int i = 0; i < n; ++i) {
                        if (tr[x1[i]] == s1[i]) continue;
                        ooMap = false;
                        break;
                    }
                    if (!ooMap) continue;
                    tr.clear();
                    for (int i = 0; i < n; ++i) tr[s1[i]] = x1[i];
                    for (int i = 0; i < n; ++i) {
                        if (tr[s1[i]] == x1[i]) continue;
                        ooMap = false;
                        break;
                    }
                    if (!ooMap) continue;
                    char x2[10];
                    for (int i = 0; i < n; ++i) x2[i] = tr[s2[i]];
                    x2[n] = 0;
                    int k22;
                    sscanf(x2, "%d", &k22);
                    int k2 = sqrt(k22) + 0.5;
                    sprintf(x1, "%d", k2 * k2);
                    if (strcmp(x1, x2)) continue;
                    for (int k: {k1, k2}) {
                        if (k <= max) continue;
                        max = k;
                    }
                }
            }
    printf("%d\n", max * max);
    return 0;
}
