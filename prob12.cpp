#include <cstdio>
#include "lib/Prime.h"
using namespace std;

int divisorNum(int n) {
    auto a = Prime::factorize(n);
    a.emplace_back(0);
    int ret = 1, k = 2;
    for (int i = 1; i < (int)a.size(); ++i)
        if (a[i] != a[i - 1]) { ret *= k; k = 2; }
        else ++k;
    return ret;
}

int main() {
    for (int i = 2; ; ++i) {
        int a = i * (i + 1) >> 1;
        if (divisorNum(a) <= 500) continue;
        printf("%d\n", a);
        return 0;
    }
    return 0;
}
