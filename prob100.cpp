#include <cstdio>
#include <cmath>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    double c1 = 3.0 + 2.0 * sqrt(2.0);
    double c2 = (2.0 - sqrt(2.0)) * 0.125;
    double c3 = (sqrt(2.0) - 1.0) * 0.25;
    for (double i = 1.0; ; i *= c1) {
        uint64_t x1 = c2 * i + 1.0;
        uint64_t x2 = c3 * i + 1.0;
        if (x2 > 1e12) {
            printf("%" PRId64 "\n", x1);
            break;
        }
    }
    return 0;
}
