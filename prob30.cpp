#include <cstdio>
using namespace std;

int main() {
    int y5[10];
    for (int i = 0; i < 10; ++i) y5[i] = i * i * i * i * i;
    int sum = 0;
    for (int i = y5[9] * 6; i >= 10; --i) {
        char s[7];
        sprintf(s, "%d", i);
        int k = 0;
        for (int j = 0; s[j]; ++j) k += y5[s[j] - '0'];
        if (k == i) sum += i;
    }
    printf("%d\n", sum);
    return 0;
}
