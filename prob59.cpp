#include <cstdio>
using namespace std;

int main() {
    int a[3][256]{};
    auto in = fopen("data/prob59.txt", "r");
    for (int c = 0; ; ) {
        int k;
        if (fscanf(in, "%d,", &k) < 1) break;
        ++a[c][k];
        if (++c == 3) c = 0;
    }
    fclose(in);
    int sum = 0;
    for (int i = 0; i < 3; ++i) {
        int max = 0, maxI = -1;
        for (int j = 0; j < 256; ++j) {
            if (a[i][j] <= max) continue;
            max = a[i][j];
            maxI = j;
        }
        char key = maxI ^ ' ';
        for (int j = 0; j < 256; ++j)
            sum += a[i][j] * (j ^ key);
    }
    printf("%d\n", sum);
    return 0;
}
