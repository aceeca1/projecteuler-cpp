#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    mpz_class gg2(1);
    for (int i = 1; i <= 200; ++i) gg2 *= 10;
    int sum = 0;
    for (int i = 1; i <= 100; ++i) {
        mpz_class k = gg2 * i;
        mpz_class s = sqrt(k);
        if (s * s == k) continue;
        for (char j: s.get_str().substr(0, 100)) sum += j - '0';
    }
    printf("%d\n", sum);
    return 0;
}
