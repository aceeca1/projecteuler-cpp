#include <cstdio>
#include "lib/Prime.h"
using namespace std;

int main() {
    int max = 0, maxI = -1;
    for (Prime i; *i < 1000; ++i)
        for (int j = -1000; j <= 1000; ++j) {
            int k = 1;
            for (;;) {
                int v = *i + k * (j + k);
                if (v < 2 || !Prime::is(v)) break;
                ++k;
            }
            if (k > max) { max = k; maxI = (*i) * j; }
        }
    printf("%d\n", maxI);
    return 0;
}
