#include <cstdio>
#include <ctime>
using namespace std;

int main() {
    int sum = 0;
    tm date{};
    date.tm_mday = 1;
    for (int i = 1; i <= 100; ++i) {
        date.tm_year = i;
        for (int j = 0; j < 12; ++j) {
            date.tm_mon = j;
            mktime(&date);
            sum += date.tm_wday == 0;
        }
    }
    printf("%d\n", sum);
    return 0;
}
