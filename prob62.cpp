#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <unordered_map>
#include <cstring>
#include "lib/Hash.h"
using namespace std;

int main() {
    size_t k = 0;
    unordered_map<uint64_t, vector<uint64_t>> a;
    for (int i = 1; ; ++i) {
        auto m = (uint64_t)i * i * i;
        char s[20];
        sprintf(s, "%" PRId64, m);
        uint64_t hash = 0;
        for (int j = 0; s[j]; ++j) hash += Hash::h(s[j]);
        if (strlen(s) != k) {
            for (auto& i: a) {
                if (i.second.size() < 5) continue;
                printf("%" PRId64 "\n", i.second[0]);
                return 0;
            }
            a.clear();
            k = strlen(s);
        } else a[hash].emplace_back(m);
    }
}
