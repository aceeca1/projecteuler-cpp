#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

void countDigits(int* a, int n) {
    char s[10];
    sprintf(s, "%d", n);
    for (int i = 0; s[i]; ++i) ++a[s[i] - '0'];
}

bool permuteMul(int n) {
    int c2[10]{};
    countDigits(c2, n + n);
    for (int j = 3; j <= 6; ++j) {
        int cj[10]{};
        countDigits(cj, n * j);
        if (!equal(c2, c2 + 10, cj)) return false;
    }
    return true;
}

int main() {
    for (int i = 1; ; ++i) {
        if (!permuteMul(i)) continue;
        printf("%d\n", i);
        return 0;
    }
    return 0;
}
