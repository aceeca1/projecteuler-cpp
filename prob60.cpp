#include <cstdio>
#include <vector>
#include <queue>
#include <initializer_list>
#include <gmpxx.h>
#include "lib/Prime.h"
#include "lib/PollardRho.h"
using namespace std;

struct PPSet {
    static vector<PPSet>& getA() {
        static vector<PPSet> a;
        return a;
    }

    Prime v;
    string vS;
    int p, n, prio;
    bool valid;

    bool isPrime(const string& s) {
        if (s.size() <= 8) {
            int sI;
            sscanf(s.c_str(), "%d", &sI);
            return Prime::is(sI);
        }
        return PollardRho::isPrime(mpz_class(s));
    }

    PPSet(Prime&& v_, int p_): v(v_), p(p_) {
        auto& a = getA();
        if (p == -1) {
            n = 1;
            prio = *v * 5;
        } else {
            n = a[p].n + 1;
            prio = a[p].prio + (*v - *a[p].v) * (6 - n);
        }
        char s[10];
        sprintf(s, "%d", *v);
        vS = s;
        valid = true;
        for (int i = p; i != -1; i = a[i].p) {
            for (auto j: {a[i].vS + vS, vS + a[i].vS}) {
                if (isPrime(j)) continue;
                valid = false;
                return;
            }
        }
    }

    bool operator<(const PPSet& rhs) const {
        return prio > rhs.prio;
    }
};


int main() {
    auto& a = PPSet::getA();
    priority_queue<PPSet> pq;
    pq.emplace(Prime(), -1);
    while (!pq.empty()) {
        a.emplace_back(pq.top());
        pq.pop();
        PPSet& ab = a.back();
        if (ab.valid && ab.n == 5) {
            printf("%d\n", a.back().prio);
            return 0;
        }
        if (ab.valid) {
            pq.emplace(move(++Prime(ab.v)), a.size() - 1);
        }
        pq.emplace(move(++Prime(ab.v)), ab.p);
    }
    return 0;
}
