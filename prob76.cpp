#include <cstdio>
#include <vector>
using namespace std;

int main() {
    vector<int> a(101);
    a[0] = 1;
    for (int i = 1; i <= 100; ++i)
        for (int j = i; j <= 100; ++j)
            a[j] += a[j - i];
    printf("%d\n", a[100] - 1);
    return 0;
}
