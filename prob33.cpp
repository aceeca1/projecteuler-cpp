#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    int a = 1, b = 1;
    for (int i1 = 1; i1 <= 9; ++i1)
        for (int i0 = 0; i0 <= 9; ++i0) {
            int i = i1 * 10 + i0, j0 = i1;
            for (int j1 = 1; j1 <= 9; ++j1) {
                int j = j1 * 10 + j0;
                if (i == j || i * j1 != j * i0) continue;
                a *= i;
                b *= j;
            }
        }
    mpz_class m = gcd(mpz_class(a), mpz_class(b));
    printf("%d\n", a / (int)m.get_ui());
    return 0;
}
