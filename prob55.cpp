#include <cstdio>
#include <string>
#include <algorithm>
#include <gmpxx.h>
using namespace std;

struct Lychrel {
    static bool is(mpz_class a) {
        string aS = a.get_str();
        for (int i = 0; i < 50; ++i) {
            string aSR(aS.rbegin(), aS.rend());
            mpz_class aR(aSR, 10);
            a += aR;
            aS = a.get_str();
            if (equal(aS.begin(), aS.end(), aS.rbegin())) return false;
        }
        return true;
    }
};

int main() {
    int count = 0;
    for (int i = 1; i < 10000; ++i) {
        if (!Lychrel::is(i)) continue;
        ++count;
    }
    printf("%d\n", count);
    return 0;
}
