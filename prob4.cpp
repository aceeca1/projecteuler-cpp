#include <cstdio>
#include "lib/Palindrome.h"
using namespace std;

int main() {
    int max = 0;
    for (int i1 = 100; i1 < 1000; ++i1)
        for (int i2 = 100; i2 < 1000; ++i2) {
            int n = i1 * i2;
            if (Palindrome::is<10>(n) && n > max) max = n;
        }
    printf("%d\n", max);
    return 0;
}
