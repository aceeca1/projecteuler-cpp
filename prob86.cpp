#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int pS = 0;
    for (int i = 1; ; ++i) {
        int i2 = i * i;
        for (int p = i + i; p >= 2; --p) {
            int q = p * p + i2;
            int qS = sqrt(q);
            if (qS * qS != q) continue;
            int uB = p >> 1;
            int lB = p - i;
            if (lB < 1) lB = 1;
            pS += uB - lB + 1;
        }
        if (pS <= 1000000) continue;
        printf("%d\n", i);
        return 0;
    }
    return 0;
}
