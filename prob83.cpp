#include <cstdio>
#include "lib/ShortPath.h"
using namespace std;

int main() {
    vector<vector<int>> a(80, vector<int>(80));
    auto in = fopen("data/prob81.txt", "r");
    for (int i = 0; i < 80; ++i)
        for (int j = 0; j < 80; ++j) fscanf(in, "%d,", &a[i][j]);
    fclose(in);
    SPGraph g;
    vector<vector<int>> b(80, vector<int>(80));
    for (int i = 0; i < 80; ++i)
        for (int j = 0; j < 80; ++j) {
            b[i][j] = g.addVertex();
            if (j != 0) {
                g.addEdge(b[i][j - 1], b[i][j], a[i][j]);
                g.addEdge(b[i][j], b[i][j - 1], a[i][j - 1]);
            }
            if (i != 0) {
                g.addEdge(b[i - 1][j], b[i][j], a[i][j]);
                g.addEdge(b[i][j], b[i - 1][j], a[i - 1][j]);
            }
        }
    g.shortPath(0);
    printf("%d\n", g.v[g.v.size() - 1].d + a[0][0]);
    return 0;
}
