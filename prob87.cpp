#include <cstdio>
#include <vector>
#include <functional>
#include <unordered_set>
#include "lib/Prime.h"
using namespace std;

void cPrimeF(vector<int>& v, function<int(int)> f) {
    for (Prime i; ; ++i) {
        int fi = f(*i);
        if (fi > 50000000) break;
        v.emplace_back(fi);
    }
}

int main() {
    vector<int> p2, p3, p4;
    cPrimeF(p2, [](int n) { return n * n; });
    cPrimeF(p3, [](int n) { return n * n * n; });
    cPrimeF(p4, [](int n) { return n * n * (n * n); });
    unordered_set<int> col;
    for (int i2: p2)
        for (int i3: p3)
            for (int i4: p4) {
                int i = i2 + i3 + i4;
                if (i >= 50000000) continue;
                col.emplace(i);
            }
    printf("%d\n", (int)col.size());
    return 0;
}
