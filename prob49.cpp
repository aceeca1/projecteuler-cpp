#include <cstdio>
#include <cstdint>
#include <unordered_map>
#include "lib/Prime.h"
#include "lib/Hash.h"
using namespace std;

int main() {
    Prime p;
    while (*p < 1000) ++p;
    unordered_map<uint64_t, vector<int>> a;
    for (; *p < 10000; ++p) {
        char s[10];
        sprintf(s, "%d", *p);
        uint64_t h = 0;
        for (int i = 0; s[i]; ++i) {
            h += Hash::h(s[i]);
        }
        a[h].emplace_back(*p);
    }
    for (auto i: a) {
        auto &iS = i.second;
        for (int j0 = 0; j0 < (int)iS.size(); ++j0)
            for (int j1 = j0 + 1; j1 < (int)iS.size(); ++j1)
                for (int j2 = j1 + 1; j2 < (int)iS.size(); ++j2) {
                    if (iS[j0] + iS[j2] != iS[j1] + iS[j1]) continue;
                    if (iS[j0] == 1487) continue;
                    printf("%d%d%d\n", iS[j0], iS[j1], iS[j2]);
                    return 0;
                }
    }
    return 0;
}
