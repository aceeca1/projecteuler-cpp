#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    auto in = fopen("data/prob8.txt", "r");
    uint64_t b[13] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    uint64_t p = 1, max = 0;
    int e = 13, k = 0;
    for (;;) {
        char r;
        if (fscanf(in, " %c", &r) < 1) break;
        p /= b[k];
        b[k] = r - '0';
        if (!b[k]) { b[k] = 1; e = 13; }
        p *= b[k];
        if (e <= 0 && p > max) max = p;
        --e;
        k = (k + 1) % 13;
    }
    fclose(in);
    printf("%" PRId64 "\n", max);
    return 0;
}
