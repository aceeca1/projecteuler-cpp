#include <cstdio>
#include <climits>
#include "lib/Divisor.h"
using namespace std;

int main() {
    vector<int> a(1000001);
    for (int i = 0; i <= 1000000; ++i) {
        if (a[i]) continue;
        vector<int> b;
        int j = i, no = -1;
        for (; j <= 1000000 && !a[j]; --no) {
            a[j] = no;
            b.emplace_back(j);
            j = Divisor::sum(j);
        }
        if (j > 1000000 || a[j] > 0)
            for (int k: b) a[k] = INT_MAX;
        else {
            int no1 = ~a[j], cyc = ~no - no1;
            for (int k = 0; k < no1; ++k) a[b[k]] = INT_MAX;
            for (int k = no1; k < (int)b.size(); ++k) a[b[k]] = cyc;
        }
    }
    int max = 0, maxI = -1;
    for (int i = 0; i <= 1000000; ++i) {
        if (a[i] == INT_MAX || a[i] <= max) continue;
        max = a[i];
        maxI = i;
    }
    printf("%d\n", maxI);
    return 0;
}
