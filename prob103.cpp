#include <cstdio>
#include <vector>
using namespace std;

constexpr int M = 7;

struct SpSum {
    int a[M], s;

    bool check(int n, int k, int a1, int a2, int z1, int z2) {
        return k >= n ?
            a1 != a2 &&
            !(z1 < z2 && a2 <= a1) &&
            !(z2 < z1 && a1 <= a2) :
            check(n, k + 1, a1 + a[k], a2, z1 + 1, z2) &&
            check(n, k + 1, a1, a2 + a[k], z1, z2 + 1) &&
            check(n, k + 1, a1, a2,        z1, z2    );
    }

    bool put(int n, int sR) {
        if (n == M - 1) {
            a[n] = s - sR;
            return a[n] > a[n - 1] && check(n, 0, a[n], 0, 1, 0);
        }
        for (int i = n ? a[n - 1] + 1 : 1; ; ++i) {
            int left = M - n;
            if (sR + i * left + (left * (left - 1) >> 1) > s) return false;
            if (n == 3 && ((sR + i) << 1) <= s) continue;  // only for M = 7
            a[n] = i;
            if (check(n, 0, i, 0, 1, 0))
                if (put(n + 1, sR + i)) return true;
        }
    }

    SpSum() {
        for (s = 1; ; ++s) if (put(0, 0)) break;
        for (int i = 0; i < M; ++i) printf("%d", a[i]);
        printf("\n");
    }
};

int main() {
    SpSum();
    return 0;
}
