#include <cstdio>
#include <climits>
using namespace std;

int main() {
    int a = 1, b = 2000;
    int min = INT_MAX, minI = -1;
    for (;;) {
        int aS = a * (a + 1) >> 1;
        int bS = b * (b + 1) >> 1;
        int c = aS * bS;
        int e = c - 2000000;
        if (e < 0) e = -e;
        if (e < min) {
            min = e;
            minI = a * b;
        }
        if (c >= 2000000) --b; else ++a;
        if (!b) break;
    }
    printf("%d\n", minI);
}
