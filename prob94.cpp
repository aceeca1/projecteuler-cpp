#include <cstdio>
#include <cmath>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    uint64_t sum = 0;
    double c = 7.0 + 4.0 * sqrt(3.0);
    double c1 = c;
    for (;; c1 *= c) {
        uint64_t n = c1 + 2.5;
        if (n > 1000000000) break;
        sum += n;
    }
    double c2 = (2.0 - sqrt(3.0)) * c * c;
    for (;; c2 *= c) {
        uint64_t n = c2 - 1.5;
        if (n > 1000000000) break;
        sum += n;
    }
    printf("%" PRId64 "\n", sum);
    return 0;
}
