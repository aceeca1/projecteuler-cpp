#include <cstdio>
#include <string>
#include "lib/Factorial.h"
using namespace std;

int main() {
    string a("0123456789");
    auto fac = Factorial::get();
    string b;
    int k = 999999;
    while (a.size()) {
        int m = fac[a.size() - 1], i = k / m;
        k %= m;
        b += a[i];
        a.erase(i, 1);
    }
    printf("%s\n", b.c_str());
    return 0;
}
