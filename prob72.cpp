#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include "lib/Phi.h"
using namespace std;

int main() {
    Phi phi(1000000);
    uint64_t sum = 0;
    for (int i = 2; i <= 1000000; ++i) sum += phi.a[i];
    printf("%" PRId64 "\n", sum);
    return 0;
}
