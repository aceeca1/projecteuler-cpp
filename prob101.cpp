#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int64_t f(int n) {
    int64_t ans = 0;
    for (int i = 0; i <= 10; ++i)
        ans = ans * n + 1 - ((i & 1) << 1);
    return ans;
}

int main() {
    int64_t a[11], sum = 0;
    for (int i = 0; i < 10; ++i) a[i] = f(i + 1);
    for (int i = 10; i; --i) {
        for (int j = 1; j < i; ++j)
            for (int k = i; k >= j; --k) a[k] -= a[k - 1];
        a[i] = 0;
        for (int j = i; j >= 1; --j)
            for (int k = j; k <= i; ++k) a[k] += a[k - 1];
        sum += a[i];
    }
    printf("%" PRId64 "\n", sum);
    return 0;
}
