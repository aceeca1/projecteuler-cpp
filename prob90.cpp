#include <cstdio>
#include <vector>
using namespace std;

struct CubeD {
    vector<int> b;

    void put(int k, int c, int bs) {
        if (k == 6) {
            if (bs & (1 << 6)) bs |= 1 << 9;
            else if (bs & (1 << 9)) bs |= 1 << 6;
            b.emplace_back(bs);
            return;
        }
        if (c < 10) put(k + 1, c + 1, bs | (1 << c));
        if (c < 10) put(k, c + 1, bs);
    }

    CubeD() {
        put(0, 0, 0);
    }
};

int main() {
    CubeD c;
    int count = 0;
    for (int i: c.b) {
        for (int j: c.b) {
            bool can = true;
            for (int k = 1; k <= 9; ++k) {
                int k2 = k * k, p1 = k2 / 10, p2 = k2 % 10;
                bool ij = ((i >> p1) & 1) && ((j >> p2) & 1);
                bool ji = ((j >> p1) & 1) && ((i >> p2) & 1);
                if (!ij && !ji) { can = false; break; }
            }
            count += can;
        }
    }
    printf("%d\n", count >> 1);
    return 0;
}
