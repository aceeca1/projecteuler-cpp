#include <cstdio>
#include <cstring>
#include <gmpxx.h>
using namespace std;

int main() {
    int count = 0;
    for (int i = 1; i <= 9; ++i) {
        mpz_class prod(i);
        for (int j = 1; ; ++j) {
            auto s = prod.get_str();
            if ((int)s.size() < j) break;
            if ((int)s.size() == j) ++count;
            prod *= i;
        }
    }
    printf("%d\n", count);
    return 0;
}
