#include <cstdio>
#include "lib/Factorial.h"
using namespace std;

int main() {
    auto fac = Factorial::get();
    int sum = 0;
    for (int i = fac[9] * 7; i >= 10; --i) {
        char s[10];
        sprintf(s, "%d", i);
        int a = 0;
        for (int p = 0; s[p]; ++p) a += fac[s[p] - '0'];
        if (i != a) continue;
        sum += i;
    }
    printf("%d\n", sum);
    return 0;
}
