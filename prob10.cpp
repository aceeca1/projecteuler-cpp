#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include "lib/Prime.h"
using namespace std;

int main() {
    uint64_t sum = 0;
    for (Prime i; *i <= 2000000; ++i) sum += *i;
    printf("%" PRId64 "\n", sum);
    return 0;
}
