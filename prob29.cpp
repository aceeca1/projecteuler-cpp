#include <cstdio>
#include <unordered_set>
#include <string>
#include <gmpxx.h>
using namespace std;

int main() {
    unordered_set<string> a;
    for (int i = 2; i <= 100; ++i) {
        mpz_class n = i;
        for (int j = 2; j <= 100; ++j) {
            n *= i;
            a.emplace(n.get_str());
        }
    }
    printf("%d\n", (int)a.size());
    return 0;
}
