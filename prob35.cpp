#include <cstdio>
#include <string>
#include <unordered_set>
#include "lib/Prime.h"
using namespace std;

string rotate(string s, int n) {
    n %= s.size();
    return s.substr(n) + s.substr(0, n);
}

struct CircularPrime {
    unordered_set<string> a;

    void sieve(int k) {
        for (auto i = a.begin(); i != a.end(); ) {
            if (!a.count(rotate(*i, k))) i = a.erase(i);
            else ++i;
        }
    }

    CircularPrime() {
        for (Prime i; *i < 1000000; ++i) {
            char s[10];
            sprintf(s, "%d", *i);
            a.emplace(s);
        }
        sieve(4); sieve(2); sieve(1);
    }
};

int main() {
    printf("%d\n", (int)CircularPrime().a.size());
    return 0;
}
