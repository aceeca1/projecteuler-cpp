objects := $(patsubst %.cpp, %, $(wildcard *.cpp))

all: $(objects)

$(objects): %: %.cpp
	g++ -std=c++11 -Ofast -lgmp -lgmpxx -Wall -Wextra -Werror -Wfatal-errors -o $@ $<

clean:
	rm -rf $(objects)
