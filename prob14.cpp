#include <cstdio>
#include <bitset>
#include <cstdint>
using namespace std;

int main() {
    bitset<1000000> a;
    int max = 0, maxI = -1;
    for (int i = 999999; i > 0; --i) {
        if (a[i]) continue;
        int z = 1;
        for (uint64_t k = i; k != 1; ++z) {
            if (k & 1) k = k * 3 + 1;
            else k >>= 1;
            if (k < 1000000) a.set(k);
        }
        if (z > max) { max = z; maxI = i; }
    }
    printf("%d\n", maxI);
    return 0;
}
