#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    uint64_t sum = 0;
    for (int i = 1; i <= 1000; ++i) {
        uint64_t prod = i;
        for (int j = 2; j <= i; ++j) prod = prod * i % 10000000000;
        sum += prod;
    }
    printf("%" PRId64 "\n", sum % 10000000000);
    return 0;
}
