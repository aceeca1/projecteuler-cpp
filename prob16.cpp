#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    auto a = mpz_class(1) <<= 1000;
    int sum = 0;
    for (auto i : a.get_str()) sum += i - '0';
    printf("%d\n", sum);
    return 0;
}
