#include <cstdio>
#include "lib/MaxPathSum.h"
using namespace std;

int main() {
    printf("%d\n", MaxPathSum{15, "data/prob18.txt"}());
    return 0;
}
