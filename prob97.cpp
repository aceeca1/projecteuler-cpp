#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    mpz_class a;
    mpz_powm_ui(
        a.get_mpz_t(),
        mpz_class(2).get_mpz_t(),
        7830457,
        mpz_class(10000000000).get_mpz_t());
    a = (a * 28433 + 1) % 10000000000;
    gmp_printf("%Zd\n", a.get_mpz_t());
    return 0;
}
