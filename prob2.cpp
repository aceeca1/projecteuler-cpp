#include <cstdio>
#include "lib/Fibonacci.h"
using namespace std;

int main() {
    int sum = 0;
    for (Fibonacci<int> i; *i <= 4000000; ++i)
        if (!(*i & 1)) sum += *i;
    printf("%d\n", sum);
    return 0;
}
