#include <cstdio>
#include "lib/Prime.h"
using namespace std;

int main() {
    int s3 = 1, s4 = 0, idx4 = 1;
    for (int s1 = 2; ; s1 += 2) {
        for (int s2 = 0; s2 < 4; ++s2) {
            s3 += s1;
            if (Prime::is(s3)) ++s4;
            ++idx4;
        }
        if (s4 * 10 < idx4) {
            printf("%d\n", s1 + 1);
            return 0;
        }
    }
    return 0;
}
