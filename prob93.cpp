#include <cstdio>
#include <vector>
#include <initializer_list>
#include <unordered_set>
#include <functional>
using namespace std;

struct ArithExp {
    vector<double> a;
    unordered_set<int> v;

    void visit() {
        if (a.size() == 1) {
            int a0 = a[0] + 0.5;
            if (a0 == a[0] && a0 >= 1) v.emplace(a0);
        }
        for (int i = 0; i < (int)a.size(); ++i) {
            double ai = a[i];
            a[i] = a.back();
            a.pop_back();
            for (int j = i; j < (int)a.size(); ++j) {
                double aj = a[j];
                a[j] = a.back();
                a.pop_back();
                for (double k: {
                    ai + aj, ai - aj, aj - ai,
                    ai * aj, ai / aj, aj / ai
                }) {
                    a.emplace_back(k);
                    visit();
                    a.pop_back();
                }
                a.emplace_back(a[j]);
                a[j] = aj;
            }
            a.emplace_back(a[i]);
            a[i] = ai;
        }
    }

    ArithExp(const vector<double>& a_): a(a_) { visit(); }

    int from1() {
        int k = 1;
        while (v.count(k)) ++k;
        return k;
    }
};

struct DComb4 {
    vector<double> a;
    typedef function<void(const vector<double>&)> func;
    func f;

    void foreach(int n, int k) {
        if (k == 4) { f(a); return; }
        if (n == 10) return;
        a.emplace_back(n);
        foreach(n + 1, k + 1);
        a.pop_back();
        foreach(n + 1, k);
    }

    DComb4(func f_): f(f_) { foreach(1, 0); }
};

int main() {
    int max = 0;
    vector<double> maxI;
    DComb4([&](const vector<double> & a) {
        ArithExp b(a);
        int from1 = b.from1();
        if (from1 <= max) return;
        max = from1;
        maxI = a;
    });
    printf("%.0f%.0f%.0f%.0f\n", maxI[0], maxI[1], maxI[2], maxI[3]);
    return 0;
}
