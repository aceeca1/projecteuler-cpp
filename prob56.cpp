#include <cstdio>
#include <gmpxx.h>
using namespace std;

int main() {
    int max = 0;
    for (int i = 1; i <= 100; ++i) {
        mpz_class a = i;
        for (int j = 1; j <= 100; ++j) {
            int sum = 0;
            for (char i: a.get_str()) sum += i - '0';
            if (sum > max) max = sum;
            a *= i;
        }
    }
    printf("%d\n", max);
    return 0;
}
