#include <cstdio>
#include "lib/Phi.h"
using namespace std;

int main() {
    double max = 0;
    int maxI = -1;
    Phi phi(1000000);
    for (int i = 2; i <= 1000000; ++i) {
        double ratio = (double)i / phi.a[i];
        if (ratio <= max) continue;
        max = ratio;
        maxI = i;
    }
    printf("%d\n", maxI);
}
