#include <cstdio>
#include <complex>
#include "lib/CG2D.h"
using namespace std;

bool readTri(vector<Point>& tr) {
    tr.resize(4);
    for (int i = 0; i < 3; ++i) {
        int x, y;
        if (scanf("%d,%d,", &x, &y) < 2) return false;
        tr[i] = Point(x, y);
    }
    tr[3] = tr[0];
    return true;
}

int main() {
    vector<Point> tr;
    int ans = 0;
    while (readTri(tr)) {
        if (cross(tr[1] - tr[0], tr[2] - tr[0]) < 0) swap(tr[1], tr[2]);
        ans += isInside(Point(), tr);
    }
    printf("%d\n", ans);
    return 0;
}
