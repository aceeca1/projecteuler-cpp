#include <cstdio>
#include <gmpxx.h>
using namespace std;

extern const char *data13;

int main() {
    auto in = fopen("data/prob13.txt", "r");
    mpz_class sum = 0;
    for (int i = 0; i < 100; ++i) {
        char s[51];
        fscanf(in, "%s", s);
        sum += mpz_class(s);
    }
    fclose(in);
    char s[53];
    gmp_sprintf(s, "%Zd", sum);
    s[10] = 0;
    printf("%s\n", s);
    return 0;
}
