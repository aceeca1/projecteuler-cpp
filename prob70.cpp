#include <cstdio>
#include <algorithm>
#include "lib/Phi.h"
using namespace std;

int main() {
    double min = 1.0 / 0.0;
    int minI = -1;
    Phi phi(10000000);
    for (int i = 2; i < 10000000; ++i) {
        int p = phi.a[i];
        if ((p - i) % 9) continue;
        double ratio = (double)i / p;
        if (ratio >= min) continue;
        int c[10]{};
        char s[10];
        sprintf(s, "%d", p);
        for (int j = 0; s[j]; ++j) ++c[s[j] - '0'];
        sprintf(s, "%d", i);
        for (int j = 0; s[j]; ++j) --c[s[j] - '0'];
        if (any_of(c, c + 10, [](int n) { return n; })) continue;
        min = ratio;
        minI = i;
    }
    printf("%d\n", minI);
    return 0;
}
